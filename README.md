# README #

### What is this repository for? ###
* Python repository (based on pymatgen) to create and analyze oxide structures with point defects
* version: 0.1

### How do I get set up? ###

* The file analyzeStructure3D.py is a self-containing module that contains classes to create and analyze perovskite structures.
* Dependencies: Pymatgen