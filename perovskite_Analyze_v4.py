#  Author: Jonathan Anchell and Janakiraman Balachandran
#  Center for Nanophase Materials Sciences, Oak Ridge National Laboratory, Oak Ridge, TN.

import os
import sys
import argparse
import re
import pymatgen as mg
import numpy as np
import operator
import matplotlib.pyplot as plt
import math
from popen2 import popen2
import json


class lineInput:
    def __init__(self, inputLine):
        self.input = inputLine
        self.atom_type = re.split('] ', str(inputLine[1][0]))[-1]

        coordinate_string = str(self.input[1][0])
        coords = coordinate_string.split()
        self.coords_xyz = [float(coords[1]), float(coords[2]), float(re.split(']', coords[3])[0])]

    def get_atom_coords(self):
        return self.coords_xyz

    def get_atom_type(self):
        return self.atom_type


def get_dopant_list():
    dopant_list = ['Al', 'Dy', 'Er', 'Ga', 'Gd', 'Ho', 'In', 'La', 'Nd', 'Pm', 'Sc', 'Sm', 'Y']
    return dopant_list


def get_reference_hydrogen(poscar):
    """Returns index of H.  Hard-coded under the assumption H is last atom in POSCAR.

    """

    reference_H_index = poscar.num_sites - 1

    return reference_H_index


def get_neighbors(reference_site, neighborSpecies, wet_dry):
    """Returns tuple of form: [(neighbor1_index, distance from neighbor1_index to reference_index), \
    (neighbor2_index, distance from neighbor2_index to reference_index), ...]
    Tuple is sorted by distance to reference_index.

    """

    neighbor_to_ref_dict= {}
    if wet_dry == 'wet':
        neighbors_to_refAtomindex = poscar_wet.get_neighbors(reference_site, 5, include_index=True)

    if wet_dry == 'dry':
        neighbors_to_refAtomindex = poscar_dry.get_neighbors(reference_site, 5, include_index=True)

    count = 0
    for entry in range(1, len(neighbors_to_refAtomindex)):
        cord_index_list = [neighbors_to_refAtomindex[count][0], neighbors_to_refAtomindex[count][2]]
        neighbor_to_ref_dict[neighbors_to_refAtomindex[count][1]] = cord_index_list
        count += 1

    sorted_neighbor_tuple_all= sorted(neighbor_to_ref_dict.items(), key=operator.itemgetter(0))

    sorted_neighbor_tuple = []
    for i in range(0, len(sorted_neighbor_tuple_all)):
        uline = lineInput(sorted_neighbor_tuple_all[i])
        neighbor_atom_type = uline.get_atom_type()
        if neighborSpecies == neighbor_atom_type:
            sorted_neighbor_tuple.append(sorted_neighbor_tuple_all[i])

    return sorted_neighbor_tuple


def get_normalized_vector(cartesian_coord1, cartesian_coord2):
    """Returns normalized vector pointing from atom1 to atom2.

    """

    coords = ['x', 'y', 'z']

    #  Create unnormalized vector.
    unnormalized_vector = {}
    count = 0
    for coord in coords:
        unnormalized_vector[coord] = cartesian_coord1[count] - cartesian_coord2[count]
        count += 1

    #  Create normalized vector.
    vector_length = (unnormalized_vector['x']**2 +
                     unnormalized_vector['y']**2 + unnormalized_vector['z']**2)**.5

    normalized_vector = {}
    for coord in coords:
        normalized_vector[coord] = unnormalized_vector[coord] / vector_length

    return normalized_vector


def get_dot_product(vector1, vector2):
    """Returns dot product of two vectors

    """

    dot_product = vector1['x'] * vector2['x'] + vector1['y'] * vector2['y'] + vector1['z'] * vector2['z']

    return dot_product


def get_angle(dot_prodcut):
    """Returns angle given by dot product.

    """

    angle_degrees = math.degrees(math.acos(dot_prodcut))

    return angle_degrees


def get_distance(cart_start, cart_end):
    """Given two sets of coordinates, calculates distance."

    """

    dist_x = cart_end[0] - cart_start[0]
    if abs(dist_x) > 10:
        dist_x += 12.5918698311000021

    dist_y = cart_end[1] - cart_start[1]
    if abs(dist_y) > 10:
        dist_y += 12.5918698311000021

    dist_z = cart_end[2] - cart_start[2]
    if abs(dist_z) > 10:
        dist_z += 12.5918698311000021

    distance = ((dist_x)**2 + (dist_y)**2 + (dist_z)**2)**.5

    return distance


def get_average_displacement(disp_list):
    """Calculates average displacement given a list of distances.  Hard-coded to find
    displacement of first six entries.

    """

    averageDisplacement = (disp_list[0] + disp_list[1] + disp_list[2]
                           + disp_list[3] + disp_list[4] + disp_list[5]) / 6

    return averageDisplacement


def print_angle_results(count_1, count_2, O1_index, dopant_index, O_index, angle, atom):
    """Prints all angles calculated by get_angle.

    """

    if count_2 == 1:
        print ("")
        print ("Dopant: %s%s  index: %d" % (atom, count_1, dopant_index + 1))
    print ("Angle between atom %d - atom %d  and atom %d - atom %d:  %s"
           % (O1_index + 1, dopant_index + 1, O_index + 1, dopant_index + 1, angle))

    return 0


def plot_angle_graph(dopantList, average_angle_total_list, folderPath, cluster, no_cluster):
    """Prints average angle graph.

    """

    if cluster:
        plt.figure(1)
        xVal = np.arange(0, len(dopantList), 1)
        plt.plot(xVal, average_angle_total_list, 'k-o', label='Clustering')
        plt.title('Average Angle with Clustering')
        plt.xticks(xVal, dopantList, rotation=45)
        plt.gcf().subplots_adjust(bottom=0.15)
        plt.xlabel('Dopant Type')
        plt.ylabel('Angle (degrees)')
        plt.savefig(os.path.join(folderPath, 'angle_clustering.eps'), format='eps', dpi=1000)
        plt.close

    elif no_cluster:
        plt.figure(2)
        xVal = np.arange(0, len(dopantList), 1)
        plt.plot(xVal, average_angle_total_list, 'k-o', label='No-Clustering')
        plt.title('Average Angle without Clustering')
        plt.xticks(xVal, dopantList, rotation=45)
        plt.gcf().subplots_adjust(bottom=0.15)
        plt.xlabel('Dopant Type')
        plt.ylabel('Angle (degrees)')
        plt.savefig(os.path.join(folderPath, 'angle_no_clustering.eps'), format='eps', dpi=1000)
        plt.close

    return 0


def print_displacement_results(dopant, average_displacement):
    """Prints all displacements calculated by caclulate_average_displacement.

    """

    print ("AVERAGE DISPLACEMENT: %s" % average_displacement)

    return 0


def plot_disp_graph(dopantList, average_displacement_list, folderPath, cluster, no_cluster):
    """Print average displacement graph.

    """

    if cluster:
        plt.figure(3)
        xVal = np.arange(0, len(dopantList), 1)
        plt.plot(xVal, average_displacement_list, 'k-o')
        plt.xticks(xVal, dopantList, rotation=45)
        plt.gcf().subplots_adjust(bottom=0.15)
        plt.xlabel('Dopant Type')
        plt.ylabel('Displacement (A)')
        plt.title('Average Displacement Cluster')
        plt.savefig(os.path.join(folderPath, 'disp_clustering.eps'), format='eps', dpi=1000)
        plt.close

    if no_cluster:
        plt.figure(4)
        xVal = np.arange(0, len(dopantList), 1)
        plt.plot(xVal, average_displacement_list, 'k-o')
        plt.xticks(xVal, dopantList, rotation=45)
        plt.gcf().subplots_adjust(bottom=0.15)
        plt.xlabel('Dopant Type')
        plt.ylabel('Displacement (A)')
        plt.title('Average Displacement no Cluster')
        plt.savefig(os.path.join(folderPath, 'disp_no_clustering.eps'), format='eps', dpi=1000)
        plt.close
    return 0


def print_energy_results(Dopants_NoCluster, energyDiff_NoCluster, energyDiff_Cluster, cluster, no_cluster):
    """Prints all energy differences.

    """

    if no_cluster:
        print ""
        count = 0
        print "No Cluster"
        for dopant in Dopants_NoCluster:
            dopant = re.split('_', dopant)[-1]
            print ("Dopant: %s  Energy Difference: %s" % (dopant, energyDiff_NoCluster[count]))
            count += 1

    if cluster:
        count = 0
        print ""
        print "Cluster"
        for dopant in Dopants_NoCluster:
            dopant = re.split('_', dopant)[-1]
            print ("Dopant: %s  Energy Difference: %s" % (dopant, energyDiff_Cluster[count]))
            count += 1

    return 0


def plot_energy_graph(Dopants_NoCluster, Dopants_Cluster, energyDiff_NoCluster, energyDiff_Cluster, dirName):
    """Print energy differences graph.

    """

    plt.figure(3)
    xVal = np.arange(0, len(Dopants_NoCluster), 1)
    plt.plot(xVal, energyDiff_NoCluster, 'k-o', label='NO-Clustering')
    xVal = np.arange(0, len(Dopants_Cluster), 1)
    plt.plot(xVal, energyDiff_Cluster, 'r-^', label='Clustering')

    plt.xticks(xVal, Dopants_Cluster, rotation=45)
    axes = plt.gca()
    axes.set_xlim([-1, 13])
    axes.set_ylim([3.4, 6.4])

    plt.legend()
    plt.xlabel('Dopant Type')
    plt.ylabel('Adsorption Energy (eV)')
    plt.gcf().subplots_adjust(bottom=0.15)
    plt.yscale('log')
    plt.savefig(os.path.join(dirName, 'FreeEnergy_Tot.eps'), format='eps', dpi=1000)
    plt.close()

    return 0


def main():
    """Main Execution Function"""

    parser = argparse.ArgumentParser(description="Calculates various parameters of the perovskite structure.  Currently hard-coded for BZO. \
                                                 Must specify a task, e.g. '--angle_print', as well as a cell type '--cluster' or 'no_cluster'.")

    parser.add_argument(
        "--angle_print",
        dest='angle_print',
        action='store_true',
        help='Prints angle results and plots graph.'
    )

    parser.add_argument(
        "--disp_print",
        dest='disp_print',
        action='store_true',
        help='Prints displacement results and plots graph.'
    )

    parser.add_argument(
        "--energy_print",
        dest='energy_print',
        action='store_true',
        help='Prints energy difference results and plots graph.'
    )

    parser.add_argument(
        "--run_all_print",
        dest='run_all_print',
        action='store_true',
        help='Run all calculations and print results.'
    )

    parser.add_argument(
        "--angle_no_print",
        dest='angle_no_print',
        action='store_true',
        help='Calculate average angles and plot graph.  Results not shown.'
    )

    parser.add_argument(
        "--disp_no_print",
        dest='disp_no_print',
        action='store_true',
        help='Calculate average displacements and plot graph.  Results not shown.'
    )

    parser.add_argument(
        "--energy_no_print",
        dest='energy_no_print',
        action='store_true',
        help='Calculate energy difference and plot graph.  Results not shown.'
    )

    parser.add_argument(
        "--run_all_no_print",
        dest='run_all_no_print',
        action='store_true',
        help='Run all calculations and plot graphs.  Results not shown.'
    )

    parser.add_argument(
        "--no_cluster",
        dest='no_cluster',
        action='store_true',
        help='Run no-cluster calculations only.'
    )

    parser.add_argument(
        "--cluster",
        dest='cluster',
        action='store_true',
        help='Run cluster calculations only.'
    )

    #  Read options passed to this script.
    options = parser.parse_args()

    angle_print = options.angle_print
    disp_print = options.disp_print
    energy_print = options.energy_print
    angle_no_print = options.angle_no_print
    disp_no_print = options.disp_no_print
    energy_no_print = options.energy_no_print
    run_all_no_print = options.run_all_no_print
    run_all_print = options.run_all_print
    no_cluster = options.no_cluster
    cluster = options.cluster

    #  Verify correctness of input options.
    if not angle_print and not disp_print and not angle_no_print and not \
            disp_no_print and not run_all_no_print and not run_all_print\
            and not energy_print and not energy_no_print:
        print ("Please specify an output option.")
        print ("For a list of output options type: 'python cell_analyze.py --help'.")
        sys.exit(1)

    if not cluster and not no_cluster:
        print "Please specify '--cluster' or '--no_cluster'."
        sys.exit(1)

    if cluster and no_cluster:
        print "'--cluster' and '--no_cluster' may not be specified at the same time.  Sorry."
        sys.exit(1)

    #  Create list of dopants.
    dopantList = get_dopant_list()

    #  Initialize average angle and displacement lists.
    average_angle_list = []
    average_angle_total_list = []
    average_displacement_list = []

    #  Open directory corresponding to cluster or no_cluster options.
    for dopant in dopantList:
        M_dopant = 'M_' + dopant
        folderPath = '/Users/j6x/Documents/Data/Dopants/'
        if no_cluster:
            systemType = 'bzo_sc333_dopant_1H/'
        elif cluster:
            systemType = 'bzo_sc333_dopantcluster_1H/'
        folderPathSystem = folderPath + systemType
        folderPathDopant = folderPathSystem + M_dopant

        #  Hard-code names of poscar decks.
        global poscar_wet
        poscar_wet = mg.Structure.from_file(os.path.join(folderPathDopant, 'POSCAR_03'))

        global poscar_dry
        poscar_dry = mg.Structure.from_file(os.path.join(folderPathDopant, 'POSCAR_01'))

        #  Create global list of sites.
        global sites
        wet_sites = poscar_wet.sites
        dry_sites = poscar_dry.sites

        #  Find index and site of H atom.
        reference_H_index = get_reference_hydrogen(poscar_wet)
        reference_H_site = wet_sites[reference_H_index]

        #  Find Oxygen neighbors of Hydrogen sorted by distance.
        O_neighbors_to_ref_H = get_neighbors(reference_H_site, "O", 'wet')

        #  Get index, site and coordinates of closest Oxygen to Hydrogen.
        O1_index = O_neighbors_to_ref_H[0][1][1]
        O1_site = O_neighbors_to_ref_H[0][1][0]
        O1_coords = lineInput(O_neighbors_to_ref_H[0]).get_atom_coords()

        #  RUN ANGLE CALCULATIONS IF SPECIFIED.
        if angle_print or angle_no_print or run_all_no_print or run_all_print:

            #  Find dopant neighbors of O1_index sorted by distance.
            dopant_neighbors_to_O1 = get_neighbors(O1_site, dopant, 'wet')

            angle_list = []
            count_1 = 0
            for neighbor in dopant_neighbors_to_O1[:2]:

                #  Get dopant index, site and coordinates.
                dopant_index = dopant_neighbors_to_O1[count_1][1][1]
                dopant_site = dopant_neighbors_to_O1[count_1][1][0]
                dopant_coords = lineInput(dopant_neighbors_to_O1[count_1]).get_atom_coords()

                #  Find Oxygen neighbors of dopant_index sorted by distance.
                O_neighbors_to_dopant_index = get_neighbors(dopant_site, "O", 'wet')

                #  Get normalized vector between O1_index and dopant_index.
                O1_dopant_vector = get_normalized_vector(O1_coords, dopant_coords)

                count_1 += 1
                count_2 = 0
                for oxygen in O_neighbors_to_dopant_index:
                    #  Get index and coordinates for oxygen atoms around dopant.
                    O_index = oxygen[1][1]
                    O_coords = lineInput(oxygen).get_atom_coords()

                    if O_index != O1_index and count_2 < 5:
                        count_2 += 1
                        #  Get normalized vector between dopant and each of its 5 \
                        #  closest oxygen neighbors (excluding O1).
                        O_dopant_vector = get_normalized_vector(O_coords, dopant_coords)

                        #  Get dot product of O1_dopant_vector with O_dopant_vector.
                        O1Dopant_dot_ODopant = get_dot_product(O1_dopant_vector, O_dopant_vector)

                        #  Get angle between O1_dopant_vector and O_dopant_vector.
                        angle = get_angle(O1Dopant_dot_ODopant)

                        #  Get average angle.
                        angle_list.append(angle)

                        #  Get the average angle for a single dopant atom.  e.g. If there are 8 'Y', there will be one \
                        #  average_angle_one_dopant per each 'Y'.
                        if count_2 == 5:
                            average_angle_one_dopant = (angle_list[-1] + angle_list[-2] + angle_list[-3] +
                                                        angle_list[-4] + angle_list[-5]) / 5

                        #  Print results per each dopant of one dopant species.
                        if angle_print or run_all_print:
                            print_angle_results(count_1, count_2, O1_index, dopant_index, O_index, angle, dopant)
                            if count_1 >= 1 and count_2 == 5:
                                print("Average angle %s%s: %s" % (dopant, count_1, average_angle_one_dopant))

                        #  Get average angle for all dopants of one species.  e.g.  If there are 8 'Y', there will \
                        #  only be one value of average_angle_total.  This is the average angle for the five oxygen \
                        #  around the nearest dopant to O1 along with the average angle for the five nearest \
                        #  oxygen around the second nearest dopant to O1.

                        if no_cluster and count_1 == 1 and count_2 == 5:
                            average_angle_total = sum(angle_list) / len(angle_list)

                        if cluster and count_1 == 2 and count_2 == 5:
                            average_angle_total = sum(angle_list) / len(angle_list)

                            #  Make list of average angles.
                            average_angle_list.append(average_angle_total)

                            #  Print average results per dopant species.
                            if run_all_print or angle_print:
                                print ("")
                                print("AVERAGE ANGLE FOR ALL %s ATOMS: %s" % (dopant, average_angle_total))

            #  Create list of average angle totals.
            average_angle_total_list.append(average_angle_total)

        #  Make plot of average angles vs dopants.
        if angle_no_print or angle_print or run_all_no_print or run_all_print:
            if len(dopantList) == len(average_angle_total_list):
                plot_angle_graph(dopantList, average_angle_total_list, folderPath, cluster, no_cluster)

        #  RUN DISPLACEMENT CALCULATIONS IF SPECIFIED.
        if disp_no_print or disp_print or run_all_no_print or run_all_print:

            reference_H_site_dry = dry_sites[reference_H_index]

            O_neighbors_to_ref_H_dry = get_neighbors(reference_H_site_dry, "O", 'dry')

            #  Get index, site and coordinates of closest Oxygen to Hydrogen.
            O1_site_dry = O_neighbors_to_ref_H_dry[0][1][0]

            #  Find Oxygen neighbors of O1_index sorted by distance.
            O_neighbors_to_O1_dry = get_neighbors(O1_site_dry, "O", 'dry')

            #  Create list of O displacements.
            displacements = []
            count = 0

            for entry in O_neighbors_to_O1_dry:
                cart_coord_dry = lineInput(entry).get_atom_coords()

                wet_site = str(wet_sites[entry[1][1]])
                wet_site_split = wet_site.split()
                cart_coord_wet = [float(wet_site_split[1]), float(wet_site_split[2]), float(re.split(']', wet_site_split[3])[0])]

                distance = get_distance(cart_coord_dry, cart_coord_wet)

                displacements.append(distance)
                count += 1

            average_displacement = get_average_displacement(displacements)
            average_displacement_list.append(average_displacement)

            #  Print displacement results.
            if disp_print or run_all_print:
                print_displacement_results(dopant, average_displacement)

            #  Make plot of average displacements vs Dopants.
            if len(dopantList) == len(average_displacement_list):
                plot_disp_graph(dopantList, average_displacement_list, folderPath, cluster, no_cluster)

    #  RUN ENERGY CALCULATIONS IF SPECIFIED.
    if run_all_no_print or run_all_print or energy_no_print or energy_print:
        folderPath = '/Users/j6x/Documents/Data/Dopants'
        jsonFile = 'freeEnergy.json'

        with open(os.path.join(folderPath, jsonFile)) as inFile:
            inputData = json.load(inFile)

        dirName = inputData["inputPath"]
        fileName = inputData["inputFile"]
        output, input = popen2('bash')
        jobstring = """find %s -type f -name %s""" % (dirName, fileName)
        input.write(jobstring)
        input.close()

        fileList = output.read().split()
        energyValDict = {}

        for file in fileList:
            energyValList = []
            systemType = re.split('/', file)[-2]
            with open(file, 'r') as f:
                for line in f:
                    lineVal = line.split()
                    pathVal = re.split('/', lineVal[0])
                    energyVal = lineVal[3]
                    dopant = [s for s in pathVal if 'M_' in s][0]
                    if 'static_01' in pathVal:
                        energyValList.append([systemType, dopant, energyVal])
            energyValDict[systemType] = energyValList

        for key, val in energyValDict.iteritems():
            systemType = key
            if systemType == 'bzo_sc333_dopant':
                Dopants_NoCluster_v1 = [s[1] for s in val]
                freeEnergy_Dry_NoCluster = [float(s[2]) for s in val]
                indexVal_NoCluster = Dopants_NoCluster_v1.index("M_Tl")
                del Dopants_NoCluster_v1[indexVal_NoCluster]
                del freeEnergy_Dry_NoCluster[indexVal_NoCluster]

            if systemType == 'bzo_sc333_dopant_1H':
                Dopants_NoCluster_v2 = [s[1] for s in val]
                freeEnergy_Wet_NoCluster = [float(s[2]) for s in val]
                indexVal_NoCluster = Dopants_NoCluster_v2.index("M_Tl")
                del Dopants_NoCluster_v2[indexVal_NoCluster]
                del freeEnergy_Wet_NoCluster[indexVal_NoCluster]

            if systemType == 'bzo_sc333_dopantcluster':
                Dopants_Cluster_v1 = [s[1] for s in val]
                freeEnergy_Dry_Cluster = [float(s[2]) for s in val]
                index_Cluster = Dopants_Cluster_v1.index('M_Tl')
                del Dopants_Cluster_v1[index_Cluster]
                del freeEnergy_Dry_Cluster[index_Cluster]

            if systemType == 'bzo_sc333_dopantcluster_1H':
                Dopants_Cluster_v2 = [s[1] for s in val]
                freeEnergy_Wet_Cluster = [float(s[2]) for s in val]
                index_Cluster = Dopants_Cluster_v2.index('M_Tl')
                del Dopants_Cluster_v2[index_Cluster]
                del freeEnergy_Wet_Cluster[index_Cluster]

        energyDiff_NoCluster = (np.asarray(freeEnergy_Dry_NoCluster) + -0.11082602E+01) - np.asarray(freeEnergy_Wet_NoCluster)
        energyDiff_Cluster = (np.asarray(freeEnergy_Dry_Cluster) + -0.11082602E+01) - np.asarray(freeEnergy_Wet_Cluster)

        #  Print energy difference results.
        if run_all_print or energy_print:
            print_energy_results(Dopants_NoCluster_v1, energyDiff_NoCluster, energyDiff_Cluster, cluster, no_cluster)

        #  Plot energy differences vs dopants.
        plot_energy_graph(Dopants_NoCluster_v2, Dopants_Cluster_v1, energyDiff_NoCluster, energyDiff_Cluster, dirName)

if __name__ == '__main__':
    EXIT_CODE = main()
    sys.exit(EXIT_CODE)